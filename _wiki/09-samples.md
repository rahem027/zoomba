# Samples

## Logical 

### Permutation of A List Containing Repeated Items
Given we have a list containing repeated items, generate 
all permutations of the list. We solve the problem by double mapping:

    l = [ 'a' , 'a' , 'b' , 'c' ,'d' ]
    r = [0:#|l|]
    permutations = set()
    join( @ARGS = list(r) as { l } ) where {
        continue ( #|set($.o)| != #|l| ) // mismatch and ignore 
        v = str($.o,'') as { l[$.o] }
        continue ( v @ permutations ) // if it already occurred 
        permutations += v // add them there  
        false // do not add  
    }

The idea of the problem can be found [here](https://www.careercup.com/question?id=5704589056671744). To solve that specific problem though,
only one important thing needs to be done, detect if a string 
is palindromic :

     s == s ** -1 // s is palindrome if it is equal to the reverse of it.
 
 Obviously that is not optimal, but more declarative. In a much optimal note:
 
     !exists( [0 : #|s|/2 ] ) :: { s[$.o] != s[-1 - $.o] }   
 
 This is optimal and declarative. It basically says : 
 > A string is palindrome if it is a mirror image of itself. 

### Is a permutation of String Palindrome 
The problem is [here](https://www.careercup.com/question?id=5641948569272320). The declarative way of solving it is 
by looking at the condition for palindrome. We note that, 
to be palindrome, the frequency of all the characters appearing 
in that string must be even, save one, which can be odd. Thus,
the below formulation works :

      #|select ( mset(s.toCharArray) ) :: {  $.o.value % 2 != 0 }| <= 1
     
In English, it reads, given a map which has key as the characters, and the value as the frequency of occurrences, if we select all characters having odd frequency, those should be less than equal to 1. Incidentally, that is precisely what we discussed.  
     

### Prime Factors
Find prime factors of a number. We simply modify the prime no. algorithm, 
or rather say, declaration : 

    primes = set()
    factors = dict( [2:n+1] ) as {
        continue ( exists ( primes ) where { $.o /? $.$.o } ) // not prime 
        primes += $.o
        continue ( n % $.o != 0  )
        count = 0 
        for( x = n ; $.o /? x ; x/= $.o ) { count += 1 }
        [$.o, count ] // return tuple   
    }   

     
### Find Largest and Smallest No. In a List 

From [Here](https://www.careercup.com/question?id=5737503739871232), 
and there goes the weasel :

    #(min, Max ) = minmax(L)


### Find Fibonacci No
Fibonacci no are given by the formula :

    Fib(n) = Fib(n-1) + Fib(n-2)
    Fib(0) = 0 , Fib(1) = 1 

A naive solution would be :

    def Fib(n){
       if( n == 0 ) return 0 
       if( n == 1 ) return 1 
       return Fib(n-1) + Fib(n-2)
    }

But, we can do better.  

    #(f_n,f_n_1) = lfold([2:n+1], [1,0] ) ->{
        #(h,l) = $.p
        [ h + l , h ] // return the new pair 
    }   

If you want to save varaibles even, we can still do better:
   
    #(f_n,f_n_1) = lfold([2:n+1], [1,0]) as { [ $.p.0 + $.p.1, $.p.0 ] }

    
## List Manipulation  
The problem is [here](https://www.careercup.com/question?id=5681516425248768). Given sorted array of integers, 
return the sorted array of squares of those integers (integers can be -ve) :

     sorta(L) :: { #|$.o.0| <  #|$.o.1| }
     l = list(L) -> { $.o ** 2 }


## Structural 

### Book Meeting Room 
The problem is [here](https://www.careercup.com/question?id=5688860014018560).
Given a list of conference rooms, and start and end time, 
we need to book a room. A conference room can be abstracted as :

    room = { 'name' : 'foobar' , schedules : list() }
    // schedule 
    schedule = [ start_time, end_time ] 

And thus, the program becomes:

    // find meeting rooms 
    def find_meetin_rooms( start_time, end_time ){
      possible_rooms = dict( rooms ) :: {
          room = $.o
          continue ( empty(room.schedules) ) { [ room.name , 0 ] } 
          continue( end_time <= room.schedules[0][0] ){  [ room.name , 1 ]  }
          // only when more than 1 at least 
          sorta(room.schedules) :: { $.o.0 < $.o.1 } 
          pos = index ( [1: #|room.schedules|] ) :: {  cur_inx = $.o 
              room.schedules[cur_inx-1].1 <= start_time && end_time <= room.schedules[cur_inx].0  
          }
          continue ( pos < 0 )
          [ room.name , pos ]
       } 
    }

### Is a Binary Tree 
This is [here](https://www.careercup.com/question?id=5724387312402432). Give I have a graph, 
I need to abstract it as nodes. Nodes are easy :

    node = { 'value' : null , children : list() }  

Now I have given a root node, and the problem is, 
if this is a binary tree or not. That is easy :

> Binary Tree is a graph with no loop and no more than 2 children at max per node. 

Thus, a solution is :

    // is binary tree
    def is_bin_tree( node ){
      if ( 'visited' @ node ) return false 
      node.visited = true 
      if ( size( node.children ) > 2 ) return false 
      return !( exists( node.children ) :: { !is_bin_tree( $.o ) } )
    }

To make amends for the condition that it can be a Binary Search tree, this can be easily done.



## Design 

### A Web Crawler
Crawlers are piece of software - which goes and dance around web pages starting from a particular web page. This is a graph discovery problem, 
as well as graph traversal. There are two parts to it:


  * Discover & Traverse the graph 
  * Given we are visiting a node, do some action based on the node 

[1] Is simple, while [2] can be achieved by eventing. So, here we go :

    // the node abstraction 
    node  = { 'url' : null , 'links' : list() } // list of other nodes 
    /*  populate a node structure 
     from here : https://jsoup.org/cookbook/extracting-data/example-list-links
    */
    #(o ? e ) = read( 'http://central.maven.org/maven2/org/jsoup/jsoup/1.9.2/jsoup-1.9.2.jar' )
    write( 'jsoup.jar' , o )
    assert ( load('jsoup.jar') , 'Can not load jsoup!' )
    import 'org.jsoup.Jsoup' as Jsoup 
    
    def make_node( url ){
      doc = Jsoup.connect(url).get()
      links = list( doc.select("a[href]") ) -> { $.o.attr("abs:href") }
      { 'url' : url , 'links' : links }
    }
    // visit node function 
    def visit_node( node ) {
       // do default action 
    } 
    def action1(){
       println( @ARGS.0.argument.0 ) // print the node 
    }
    //... rest are defined that way 
    visit_node.after += [ action1, action2, action2, ... ]
    explored_nodes = set()
    // the explore function 
    def explore( node ) {
        if ( node @ explored_nodes ) return // already explored 
        explored_nodes += node // now it is explored 
        visit_node( node ) // visit the node :: fire all actions 
        for ( node.links ) {
            explore( $ ) // explore the children ( rather, links ) 
        } 
    }
    
### Scraping CareerCup 
This was a fun activity, and here is a sample script that is supposed to scrape careercup. 

    load('/BigPackages/jsoup-1.9.2.jar')
    import 'org.jsoup.Jsoup' as Jsoup
    for ( page_num : [526:0] ){ // all 526 pages of it 
      url = 'https://www.careercup.com/page?n=' + page_num
      #(w ? e ) = read( url , 10000, 10000 )
      doc = Jsoup.parse( w.body )
      links = doc.select("a[href]")
      // pass a nop collector 
      from( links , [] ) :: { '<p>' @ $.o.html } -> { 
        printf('====%s\n%s\n',  $.o.attr("abs:href") ,  ($.o.getElementsByTag("p"))[0].text )
      }
      // wait some, else their server throttles it 
      thread().sleep(5000)
    }

### Most Frequented Words
The problem can be found [here](http://franklinchen.com/blog/2011/12/08/revisiting-knuth-and-mcilroys-word-count-programs/). The statement says:

>Read a file of text, determine the n most frequently used words, and print out a sorted list of those words along with their frequencies.

To do so, is easy :

    def do_wc_n ( file_path , num_words , word_def ){
      words =  tokens ( read( file_path ), word_def ) -> { $.item }
      ms = mset ( words ) -> { $.item.toLowerCase }
      h = heap( num_words , true ) :: { size ( $.item.0.value ) < size ( $.item.1.value ) }
      h = fold ( ms , h ) -> { $.partial += $.item }
      rfold ( h ) -> { printf ( '%s -> %s\n' , $.item.key, size( $.item.value ) )}
    }

But, we can make it into fully functional form, noting there are really no side effects and none of the lines changes any states :

    def do_wc_n ( file_path , num_words , word_def ){
      // shows the order : read -> tokens -> mset -> fold -> rfold 
      rfold ( fold ( mset ( tokens ( read( file_path ), word_def ) -> { $.o.toLowerCase } ) , 
        heap( num_words , true ) :: { $.l.value  < $.r.value  } ) -> { $.p += $.o }) -> { 
        println ( $.o ) }
    }


Never, ever, ever write code like that. This is to showcase that like other *awesome* languages, [IM](http://www.urbandictionary.com/define.php?term=intellectual%20masturbation) is possible in ZoomBA. But we are very practical people, and abhor such activities.  
  
## Non Sensical  

