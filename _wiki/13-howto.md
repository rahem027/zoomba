# How to Dance in ZoomBA Tune
This page is a list of *gotcha!* you may need to use ZoomBA.


## Debugging in ZoombA
If you are using Beta1 or less, all hopes are lost. If you are over beta2+, 
You can simply use the command line debugger option.

### Always On Debug
The debug command - one can insert to any ZoomBA script is: 

```
#bp [condition_when_break_point_will_hit = true ] 
```  
That is, the statement ```#bp``` inserts a break point, and the optional condition if it is specified, halts the execution at the break point, and opens up the command line REPL.
You can play around all expressions and variables there.
Thus, you can remove the ugly ```println()``` debugging mechanism.

### Sample Debugging 

Let's start with a sample code:

```
for ( [0:10] ){
  x = $ ** 2
  #bp 2 /? $ // break only when $ is divided by 2
}
```
Then, when you run it:

```
$ zmb tmp.zm 
============= With Power, Comes Responsibility ========
BreakPoint hit: at line 3, cols 3:12
============= Please use Power, Carefully..... ========
//h brings this help.
//h key_word : shows help about the keyword
//q quits REPL. In debug mode runs till next BreakPoint
//v shows variables.
//c In debug mode, clear this Breakpoint.
//r loads and runs a script from REPL.(Not Implemented)
Enjoy ZoomBA.....
(zoomba)$
0 // Integer
(zoomba)x
0 // Integer
(zoomba)
```
If you quit, with ```//q``` you would hit it again, this time:

```
(zoomba)x
4 // Integer
(zoomba)$
2 // Integer
```

Which sums it up. If you want to just get past it - and never to hit the break point again - simply clear it using ```//c``` or clear command.

### Inside the Functional Forms 

Given that ```#bp``` is a statement, one can put it inside any functional block! That should be awesome, basically that means :

```
fold ( [0:3] ) -> {
  // you can write this 
  #bp ; println($.o)
}
```
which will now break on each iteration:

```
(zoomba)$.i
1 // Integer
(zoomba)$.p
zoomba.lang.core.types.ZException$Property: p : at line 1, cols 3:3 // Property
(zoomba)$.c
0,1,2 // ZRangeList
(zoomba)
```
and you are good to go. Obviously one can programmatically insert breakpoints, but not through ZoomBA, a full Java bridge is on the way.

## How do I 

### print an object 
There are many ways. Obviously there are :
```
println(obj)
printf(obj)
```
#### As JSON 

but if you choose to see a json like output :

```
(zoomba)s = 'hi'
hi // String
(zoomba)str ( dict(s) , true )
{"serialPersistentFields" : [], "CASE_INSENSITIVE_ORDER" :  { "java.lang.String$CaseInsensitiveComparator" : "java.lang.String$CaseInsensitiveComparator@77a567e1" } , "serialVersionUID" : -6849794470754667710, "value" : [  { "java.lang.Character" : "h" } ,  { "java.lang.Character" : "i" }  ], "hash" : 0} // String
```

### Do File Operation 
The function ```file``` should be your friend. If you give it a text file, it is as if an iterator, if it is a directory, it traverses the files in the directory - returns ```java.io.File``` objects :

```
// list all files in a directory 
for ( f : file(directory) ) { println( f.name ) }
// list all lines in a file 
for ( l : file(text_file) ) { println( l ) }
```  

### Web IO 
The function ```read``` generally will help reading from web:

```
w = read('http://www.google.co.in')
println(w.status ) // HTTP status 
println(w.body) // the body 
```

### Demystify ```$``` syntax

In ZoomBA ```$``` comes in different ways.
It is the iteration variable. Here are different usage of how to use the special variable :

```
for ( [0,1,2,3] ) { println($) }  
```  
Can be used in fold like higher order function as such.





