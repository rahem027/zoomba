#!/bin/bash
ZMB_VER="0.1"
ZMB_BIN="$HOME/zoomba"
mkdir -p $ZMB_BIN
url=`curl -s "https://repo1.maven.org/maven2/org/zoomba-lang/zoomba.lang.core/$ZMB_VER/"  | grep 'onejar.jar"' | tail -n -1| cut -f2 -d'"'`
curl -s $url -o "$ZMB_BIN/zoomba.jar"
cp "$HOME/.profile" "$HOME/.profile_backup"
# imagine bash ??
echo "alias zmb='java -jar $HOME/zoomba/zoomba.jar'" >> "$HOME/.profile"
