# Basic Objects In ZoomBA

## Defining 
Objects gets defined with the keyword ( overly used ) *def* :

```js
def MyObject : { greeting : 'Hello, I am a ZoomBA Object!'  }
```
     
This creates an object prototype, named *MyObject* which has one ( String ) field named *greetings*. One can print it: 

    println( MyObject ) 
 
This will yield:

```js
    { "MyObject" : {"greeting" : "Hello, I am a ZoomBA Object!"} }
```

It should be pretty apparent that ZoomBA treats objects as JSON style property buckets, and is 100% JSON compatible. An object is, for lack of better word in ZoomBA : a Map.


### Creating and Extending an Object 

The object we defined is a prototype. Unlike it's cousin ECMA scripts, ZoomBA creates a prototype object, and any object is a prototype on it's own. One can create any object starting from, using, the prototype. 

The operator function *new* works here too, to create another object from the prototype :

     adam = new ( MyObject ) 
     println ( adam ) 

will produce :

```js
    { "MyObject" : {"greeting" : "Hello, I am a ZoomBA Object!"} }
```

There is no distinction between *adam* the first creation, and the creator, *MyObject* as such. But this is intolerable, because there has to be some difference. To do so, one can push extra properties in the *new* itself : 

```js
eve = new ( MyObject , gender='female' )
println ( eve )
```
                       
It is now obvious that the result should be :

```js
    { "MyObject" : {"gender" : "female", "greeting" : "Hello, I am a ZoomBA Object!"} }
```
    
and, so with a little bit tweaking, we can make *eve* greet properly too: 

```js
    eve = new ( MyObject , gender='female' , greeting = 'Hey, I am eve!')
    println ( eve )
```                            
produces :

```js
    { "MyObject" : {"gender" : "female", "greeting" : "Hey, I am eve!"} }
```

This is as simple as one can get. Now, a little bit more digging up:

```js
lilith = new ( eve , greeting = 'Hello, I am lilith!' )
println( lilith )
```

this does produce :

```js
    { "MyObject" : {"gender" : "female", "greeting" : "Hello, I am lilith!"} }
```
    
Hence, we can understand that the basic prototype *MyObject* will remain it's basis, and ZoomBA does not support multiple inheritance. ZoomBA is not a business data modelling language, it is a business rule modelling language. Of course, one can do - JSON style data modelling in ZoomBA, it is more *Not only SQL* than *relational* in nature.


## Initialisation 

Constructors are needed, but ZoomBA does not have them. ZoomBA has initialisers. We can only initialise an object, once it is already created. One can pass parameters to the existing prototype, as we have already seen in the *eve* and *lilith* example. 


In specificity, any arbitrary no. of key value pairs can be sent into the *new* function, and if those properties ( fields ) do exists, they would be replaced with those parameter values, but if they do not exists, they will be created, as we have seen in the previous examples.


### Default Init Function 
Of course there is a *method* that would handle the initialisation process:

```js
def MyObject : { 
   $$ : def(){
     $.greeting = 'Hello, I am a ZoomBA Object!' 
   }
}
o = new ( MyObject )
println( o.greeting )
```

The `$$` construct defines the default initialiser method. The curious symbol `$` defines the *this* or *self* in other languages. Hence, what the code is simply saying : "create a field called greeting in me, and assign the string to it". The result :

    Hello, I am a ZoomBA Object!

#### Arguments to Init    
 
Arguments can be passed into the init `$$()` function: 

```js
def MyObject : { 
   $$ : def( text ){
     $.greeting = text  
   }
}
o = new ( MyObject , 'Hi, am cool!' )
println( o.greeting )
```

This produces:

    Hi, am cool!

as expected. Like every function in ZoomBA, *init* functions are var args. Thus it is distinctly legal to do it this way :

    def MyObject : { 
       $$ : def(){
         $.greeting = ( size(@ARGS) > 0 ? @ARGS[0] : 'am no arg!' )  
       }
    }
    o = new ( MyObject )
    println( o.greeting )
    o = new ( MyObject , 'This is too cool!' )
    println( o.greeting )
    
This prints:

    am no arg!
    This is too cool!

### Classic Style Class Definition 
ZoomBA also supports classic style class definition.
To match with JS, this is how it is achieved:

```js
def X { } // this is a class with no init 
def Better{
  def $$(arg1=opt1,arg2=opt2,...){ // the init method 
     $.field1 = arg1 
     $.field2 = arg2
     ...
  }
}
``` 

## Methods

Like inits, objects can have methods. As you probably would have understood by now, *def* defines a method:

```js
    def MyObject { 
       def $$(){
         $.greeting = ( size(@ARGS) > 0 ? @ARGS[0] : 'am no arg!' )  
       }
       def my_method ( appender ){
          println( str( appender ) + ' : ' + $.greeting )
       }
    }
    o = new ( MyObject , time() )
    o.my_method( 'My creation time')
```
NOTE: You should never mix styles. Either it would be JS like dictionary based object definition, or classic definition like above.

generates:

    My creation time : 2016-12-27T12:37:38.603

Methods can call objects other methods, thus :

```js
def MyObject : { 
   method_1 : def(){
      $.method_2() // call another method
      println('I am method_1')
   }, // do not forget comma 
   method_2 : def(){
     println('I am method_2')
   }
}
o = new ( MyObject )
o.method_1( )
```

will produce :

    I am method_2
    I am method_1

### Special Methods

ZoomBA supports specific operator overloading. Thus, there are specific methods, once defined, will let you intermingle custom created objects with ZoomBA. These methods and the operators are (Yet to list all) :

* String : `$str()` 
* Equality ( == `$eq()` ) 
* HashCode : `$hc()` 

We will create a complex number class to showcase these scenarios:

```js
def Complex : {
  $$ : def(x=0,y=0){ // note the use of default arguments 
    $.x = x ; $.y = y 
  }
}
zero = new ( Complex )
println( zero )
```

#### String 

The code above generates a pretty convoluted string:

```js
    { "Complex" : {"$$" :  { "zoomba.lang.core.interpreter.ZScriptMethod" : "" } , "x" : 0, "y" : 0} }
```

This is simply too much information. What we need is much simpler stuff.
To have this default *str()* support, define the function `$str()` : 

```js
    def Complex : {
      $$ : def(x=0,y=0){
        $.x = x ; $.y = y 
      },
      $str : def(){
        str( '(' + $.x + ',' + $.y +'j)' )
      }
    }
```

and now, the same code will print:

    (0,0j)
    
#### Equality 

To compare is too human. Once there are objects, we need to compare them. To do so, let's do this : 

```js
z1 = new ( Complex )
z2 = new ( Complex )
println( z1 == z2 ) // surprise! true !
```

You would see that ZoomBA used it's brain, and made these two equal. How? Because it does a field by field comparison! But that itself is bound to be going wrong, for example, what about a reasonable good zero, with no imaginary part ? 

```js
z1 = new ( Complex , y = null )
z2 = new ( Complex )
println( z1 == z2 ) // no surprise, false :( 
```

So, we must tell the system to become smart and decide about how equality is to be defined. Thus we create rules :

* Given a no is purely real ( no y component ), it is equal to an imaginary no, having same 0 y component. 
* It is an error to have no x component

We implement these set of rules, and the `$eq()` function : 

```js
    def Complex : {
      $$ : def(x=0,y=0){
        panic( empty(x) , 'X can not be empty!' )
        $.x = x ; $.y = y 
      },
      $str : def(){
        str( '(' + $.x + ',' + $.y +'j)' )
      },
      $eq : def(other){
        if ( $.x != other.x ) return false 
        if ( empty($.y) || other.y == 0 ) return true 
        if ( $.y == 0 || empty( other.y ) ) return true 
        other.y == $.y 
      }
    }
    z1 = new ( Complex , y = null )
    z2 = new ( Complex )
    println( z1 == z2 )
```

And we are cool! This should be fine! 
Note: When *z1 == z2*, then `z1.$eq(z2)` gets called. This actually means, 
the operator *==* is to be defined as symmetric operator.

#### HashCode 

HashCodes are too important for JVM. This is how one can decide a quick and dirty way, if two objects are reasonably equal are not. A classic case, pouring them into a *set()*. 

```js
z1 = new ( Complex )
z2 = new ( Complex )
s = set( z1, z2 )
println( s )
```

as you would have expected, ZoomBA magically takes care of HashCode too. The result is pretty fascinating:

    { (0,0j) }

In this case, you do not need to do anything with the *hashCode()*, but if you really need it, you can override the default implementation by:

```js
def Complex : {
  $$ : def(x=0,y=0){
    panic( empty(x) , 'X can not be empty!' )
    $.x = x ; $.y = y 
  },
  $str : def(){
    str( '(' + $.x + ',' + $.y +'j)' )
  },
  $eq : def(other){
    if ( $.x != other.x ) return false 
    if ( empty($.y) || other.y == 0 ) return true 
    if ( $.y == 0 || empty( other.y ) ) return true 
    other.y == $.y 
  },
  $hc : def(){
    int( $.x ) * 31 + int($.y,0)  
  }
}
z1 = new ( Complex )
println ( z1.hashCode )
z2 = new ( Complex )
println ( z2.hashCode )
s = set( z1, z2 )
println( s )
```

This produces :

    0
    0
    { (0,0j) }

and we are good.

                                    