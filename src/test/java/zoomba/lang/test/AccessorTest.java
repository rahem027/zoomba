package zoomba.lang.test;

import org.apache.commons.jxpath.JXPathContext;
import org.apache.commons.jxpath.ri.model.NodePointer;
import org.junit.Assert;
import org.junit.Test;
import zoomba.lang.core.operations.Function;
import zoomba.lang.core.collections.ZList;
import zoomba.lang.core.collections.ZMap;
import zoomba.lang.core.operations.ZJVMAccess;
import zoomba.lang.core.types.ZTypes;

import java.util.Iterator;
import java.util.List;
import java.util.Map;

import static org.junit.Assert.*;
import static zoomba.lang.core.interpreter.ZMethodInterceptor.Default.myPath;


/**
 */
public class AccessorTest {

    final Object[] arr = new Object[ ]{ 1, 2, 3, 4, 5, 6 } ;

    final List list = new ZList(arr);

    @Test
    public void testIntegerIndexAccessor(){
        Function.MonadicContainer c =  ZJVMAccess.getProperty(arr,0);
        assertFalse( c.isNil() );
        assertTrue( c.value() instanceof Integer );

        c =  ZJVMAccess.getProperty(arr,arr.length-1);
        assertFalse( c.isNil() );
        assertTrue( c.value() instanceof Integer );

        c =  ZJVMAccess.getProperty(arr,arr.length );
        assertTrue( c.isNil() );
        assertTrue( c == ZJVMAccess.INVALID_PROPERTY );

        c =  ZJVMAccess.setProperty(arr, 0, 'A');
        assertFalse( c.isNil() );
        assertTrue( c == Function.SUCCESS );
        assertEquals( 'A', arr[0] );


        c = ZJVMAccess.getProperty(list,0);
        assertFalse( c.isNil() );
        assertTrue( c.value() instanceof Integer );

        c =  ZJVMAccess.setProperty(list, 1, 'B');
        assertFalse( c.isNil() );
        assertEquals( 'B', list.get(1) );


        c = ZJVMAccess.getProperty(list,"size" );
        assertFalse( c.isNil() );
        assertTrue( c.value() instanceof Integer );
        int size = (int)c.value() ;

        c =  ZJVMAccess.getProperty(list, size - 1 );
        assertFalse( c.isNil() );
        assertTrue( c.value() instanceof Integer );

        c =  ZJVMAccess.getProperty(list, size );
        assertTrue( c.isNil() );
        assertTrue( c == ZJVMAccess.INVALID_PROPERTY );

    }

    @Test
    public void testIndexer(){

        final Object[] keysNum = new Object[ ]{ 1, 2, 3, 4, 5, 6 } ;

        final Object[] valuesChar = new Object[ ]{ 'a', 'b', 'c', 'd', 'e', 'f' } ;

        final Map map = new ZMap(keysNum, valuesChar );

        Function.MonadicContainer c =  ZJVMAccess.getProperty(map,1);
        assertFalse( c.isNil() );
        assertTrue( c.value() instanceof Character );

        c = ZJVMAccess.getProperty(map,42);
        assertTrue( c.isNil() );
        assertTrue( c == ZJVMAccess.INVALID_PROPERTY );

        c = ZJVMAccess.setProperty(map,42, "42" );
        assertFalse( c.isNil() );
        assertEquals("42", map.get(42) );

        c = ZJVMAccess.getProperty(map,"size");
        assertFalse( c.isNil() );
        assertEquals(c.value(), map.size());

        c = ZJVMAccess.getProperty(map,"keys");
        assertFalse( c.isNil() );
        assertEquals(c.value(), map.keySet());

        c = ZJVMAccess.getProperty(map,"values");
        assertFalse( c.isNil() );
        assertEquals(c.value(), map.values());

        c = ZJVMAccess.getProperty(map,"entries");
        assertFalse( c.isNil() );
        assertEquals(c.value(), map.entrySet());
    }

    @Test
    public void xpathTest(){
        Object root = ZTypes.json( "samples/large_json.json", true);
        JXPathContext context = JXPathContext.newContext(root);
        Iterator iter  = context.iteratePointers("//products");
        // all of these should be valid now...
        while ( iter.hasNext() ){
            NodePointer dp = (NodePointer) iter.next();
            String path = myPath(dp);
            // get new ptr
            NodePointer p = (NodePointer)context.getPointer(path);
            Assert.assertTrue(p.isActual());
        }
    }

    @Test
    public void orderingVarArgsTest(){
        Object foo = new Object(){
            public int firstCalled = 0;
            public int secondCalled = 0;

            public void printf(Integer i, String msg, Object... args){
                secondCalled++;
                System.out.print(i + " >> ");
                System.out.printf(msg, args);
            }

            public void printf(String msg, Object... args){
                firstCalled ++;
                System.out.printf(msg, args);
            }
        };
        // 1
        ZJVMAccess.callMethod( foo, "printf" , new Object[]{ "Hello, world %n" });
        Assert.assertEquals( 1, ZJVMAccess.getProperty( foo, "firstCalled").value() );
        // 1-1
        ZJVMAccess.callMethod( foo, "printf" , new Object[]{ "Hello, world : %s %n", "foo-bar" });
        Assert.assertEquals( 2, ZJVMAccess.getProperty( foo, "firstCalled").value());

        // 2-1
        ZJVMAccess.callMethod( foo, "printf" , new Object[]{ 0, "Hello, world : %s %n", "foo-bar" });
        Assert.assertEquals( 1, ZJVMAccess.getProperty( foo, "secondCalled").value());
        Throwable throwable = null;

        try {
            ZJVMAccess.callMethod( foo, "printf" , new Object[]{  });
        } catch (Throwable t){
            throwable = t;
        }
        Assert.assertNotNull(throwable);
    }

    interface Foo{
        default int value(){
            return 42;
        }
    }

    interface Bar{
        default int value(){
            return 41;
        }
    }

    @Test
    public void j8DefaultInterfaceTest(){
        Foo f = new Foo(){};
        Bar b = new Bar(){};
        Object r = ZJVMAccess.callMethod(f, "value", new Object[]{});
        Assert.assertEquals( 42,  r );
        r = ZJVMAccess.callMethod(b, "value", new Object[]{});
        Assert.assertEquals( 41,  r );
        Function.MonadicContainer mc = ZJVMAccess.getProperty(f, "value");
        Assert.assertEquals( 42,  mc.value() );
    }
}
