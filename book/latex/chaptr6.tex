\chapter{Reusing Code}\label{code-reuse}

{\LARGE T}he tenet of ZoomBA is : \emph{``write once, forget''}.
That essentially means that the code written has to be sufficiently robust.
It also means that, we need to rely upon code written by other people.

How does this work? The system must be able to reuse $any$ code 
from Java SDK, and should be able to use any code that we ourselves wrote.
This chapter would be elaborating on this.

\begin{section}{The Import Directive}
\index{import}

\begin{subsection}{Syntax}

Most of the languages choose to use a non linear, tree oriented import.
ZoomBA focuses on minimising pain, so the import directive is linear.
The syntax is simple enough :
\index{import : java}
\begin{lstlisting}[style=zmbStyle]
import 'import_path' as unique_import_identifer
\end{lstlisting}

The directive imports the \emph{stuff} specified in the \emph{import\_path} and create 
an alias for it, which is : \\ \emph{unique\_import\_identifer},
Thus, there is no name collision at all in the imported script.
This \emph{unique\_import\_identifer} is known as the namespace.
\index{namespace}

\end{subsection}

 
\begin{subsection}{Examples}

Observe, if you want to import the class $java.lang.Integer$ :

\begin{lstlisting}[style=zmbStyle]
import 'java.lang.Integer' as Int // works, string literal 
import java.lang.Integer as Int // works, standard form 
x = 'java.lang.Integer' // well, programmatic access 
import x as Int // works, again. Try avoiding it, please.
\end{lstlisting}

This directive would import the class, and create an alias which is $Int$.
To use this class further, now, we should be using :

\begin{lstlisting}[style=zmbStyle]
Int.parseInt('20') // int : 20
Int.valueOf('20') // int : 20 
\end{lstlisting}

In the same way, one can import an ZoomBA script :
\index{import : ZoomBA Script}

\begin{lstlisting}[style=zmbStyle]
// w/o any extension it would find the script automatically 
import 'from/some/folder/awesome' as KungFuPanda
// call a function in the script 
KungFuPanda.showAwesomeness()
\end{lstlisting}

\end{subsection}
 
\end{section}

\begin{section}{Using Existing Java Classes}

\begin{subsection}{Load Jar and Create Instance}
\index{import : load()}
The example of outer class, or rather a proper class 
has been furnished already. So, we would try to import a class
which is not there in the $CLASS\_PATH$ at all.

\begin{lstlisting}[style=zmbStyle]
// put all dependencies of xmlbeans.jar in there
success = load('path/to/xmlbeans_jar_folder') // true/false 
import 'org.apache.xmlbeans.GDate' as AGDate
\end{lstlisting}

Once we have this class now, we can choose to instantiate it,
See the manual of this class \href{https://xmlbeans.apache.org/docs/2.1.0/reference/org/apache/xmlbeans/GDate.html}{here}:

\index{new()}
\begin{lstlisting}[style=zmbStyle]
//create the class instance : use new() 
gd1 = new ( AGDate, date() )
// 2016-02-18T21:13:19+05:30
// or even this works 
gd2 = new ( 'org.apache.xmlbeans.GDate' , date() )
// 2016-02-18T21:13:19+05:30
\end{lstlisting}

And thus, we just created a Java class instance.
This is how we call Java objects, in general from ZoomBA.

Calling methods now is easy:

\begin{lstlisting}[style=zmbStyle]
cal = gd1.getCalendar() // calls a method 
/* 2016-02-18T21:13:19+05:30 */
\end{lstlisting}

Note that thanks to the way ZoomBA works, a method of the form $getXyz$
is equivalent to a field call $xyz$ so :

\begin{lstlisting}[style=zmbStyle]
cal = gd1.calendar // calls the method but like a field! 
/* 2016-02-18T21:13:19+05:30 */
\end{lstlisting}

\end{subsection}


\begin{subsection}{Import Enum}
\index{import : enum}\index{enum()}
Enums can be imported just like classes.
However, to use one, one should use the $enum()$ function:

\begin{center}\begin{minipage}{\linewidth}
\begin{lstlisting}[style=zmbStyle]
// creates a wrapper for the enum 
rms = enum('java.math.RoundingMode')
rms.0 // UP
rms.UP // UP
\end{lstlisting}
\end{minipage}\end{center}

The same thing can be achieved by :

\begin{lstlisting}[style=zmbStyle]
// gets the value for the enum 
v = enum('java.math.RoundingMode',
          'UP' ) 
v = enum('java.math.RoundingMode', 0 )
\end{lstlisting}

\end{subsection}



\begin{subsection}{Import Static Field}
\index{import : static field}

Import lets you import static fields too. For example :

\begin{lstlisting}[style=zmbStyle]
// put all dependencies of xmlbeans.jar in there
import 'java.lang.System.out' as OUT
// now call println 
OUT.println("Hello,World") // prints it!
\end{lstlisting}

However, another way of achieving the same is :

\begin{lstlisting}[style=zmbStyle]
// put all dependencies of xmlbeans.jar in there
import 'java.lang.System' as SYS
// now call println 
SYS.out.println("Hello,World") // prints it!
// something like reflection
SYS['out'].println("Hello,World") // prints it!
\end{lstlisting}

\end{subsection}

\begin{subsection}{Import Inner Class or Enum}
\index{import : inner class, inner enum}
Inner classes can be accessed with the "\$" separator.
For example, observe from the SDK code of 
\href{http://grepcode.com/file/repository.grepcode.com/java/root/jdk/openjdk/8u40-b25/java/util/HashMap.java/}{HashMap},
that there is this inner static class $EntrySet$. So to import that:

\begin{lstlisting}[style=zmbStyle]
// note that $ symbol for the inner class
import 'java.util.HashMap$EntrySet' as ES
\end{lstlisting}

\end{subsection}

\end{section}

\begin{section}{Using ZoomBA Scripts}

We have already discussed how to import an ZoomBA script,
so in this section we would discuss how to create a re-usable script.

\begin{subsection}{Creating a Script}
We start with a basic script, let's call it $hello.zm$ :

\begin{lstlisting}[style=zmbStyle]
/* hello.zm */
def say_hello(arg){
   println('Hello, ' + str(arg) )
}
s = "Some one"
say_hello(s)
/* end of script */
\end{lstlisting}
\index{namespace : my}
This script is ready to be re-used.

\end{subsection}


\begin{subsection}{Relative Path}
\index{import : relative}
Suppose now we need to use this script, from another script, 
which shares the same folder as the ``hello.zm''. 
Let's call this script $caller.zm$. So, to import 
``hello.zm'' in $caller.zm$ we can do the following :

\begin{lstlisting}[style=zmbStyle]
import './hello' as Hello
\end{lstlisting}
   
but, the issue is when the runtime loads it, 
the relative path would with respect to the runtimes 
run directory. So, relative path, relative to the caller
would become a mess. To solve this problem, relative import is invented.

\begin{lstlisting}[style=zmbStyle]
import _/'hello' as Hello
Hello.say_hello('Awesome!" ) 
\end{lstlisting}

In this scenario, the runtime notices the ``$\_/$'', and 
starts looking from the directory the $caller.zm$ was loaded!    
Thus, without any $PATH$ hacks, the ZoomBA system works perfectly fine.

\end{subsection}

\begin{subsection}{Calling Functions}
\index{import : function call}

Functions can be called by using the namespace identifier, the syntax is :

\begin{lstlisting}[style=zmbStyle]
import 'some/path/file' as NS
NS.function(args,... ) 
\end{lstlisting}

If one needs to call the whole script, as a function, 
that is also possible, and that is done using the $execute()$ function:

\begin{lstlisting}[style=zmbStyle]
import 'some/path/file' as NS
NS.execute(args,... ) 
\end{lstlisting}
We will get back passing arguments to a function in a later chapter.
But in short, to call $hello.zm$, as a function, the code would be :
\index{import : script as function}

\begin{lstlisting}[style=zmbStyle]
import _/'hello' as Hello
Hello.execute() 
// or if you like succinct stuff 
Hello()// yep, good to go... 
\end{lstlisting}

Just as python has the implicit variable $\_\_name\_\_ == '\_\_main\_\_' $ to identify
if a script is running as `main' script or not, ZoomBA has the implicit variable $@SCRIPT$.
This stores the current script. Thus :

\begin{lstlisting}[style=zmbStyle]
@SCRIPT.main // true if it is main script, false it is not 
@SCRIPT.location // location of the currently running script 
\end{lstlisting}

Thus, say we have two files `a.zm' and `b.zm` as follows:

\begin{lstlisting}[style=zmbStyle]
// a.zm 
import 'b' as B 
B.say_something("hello!") // this is how you call a function 

// b.zm 
def say_something(arg){
  println("I am from B!" + arg )
}
// body will be executed only when the script is main 
if ( @SCRIPT.main ){
  say_something(@ARGS)
}
\end{lstlisting}
Note the omission of `.zm' while importing.

When we run this code, we see : `I am from B!hello!'
This is how we call functions from other imported modules.

Point to be noted that the body of the imported script gets executed while importing, 
thus, any stray method calls not protected by the `@SCRIPT.main' functionality will be executed.
For example, if we have the `b.zm' to be modified by :

\begin{lstlisting}[style=zmbStyle]
// b.zm 
println("I am being called - from B!")
def say_something(arg){
  println("I am from B!" + arg )
}
// body will be executed only when the script is main 
if ( @SCRIPT.main ){
  say_something(@ARGS)
}
\end{lstlisting}

Now, if we run the `a.zm' file we see the line "I am being called - from B!".
This has very interesting implications. It means all executable blocks will be executed, 
and all variables which are associated with the imported scripts block, will be initialised 
and accessible from the importing module. For all practical purpose, an imported module 
acts same as an object :

\begin{lstlisting}[style=zmbStyle]
// a.zm 
import 'b' as B 
println( B.TI ) // this is how you access free variables defined on imported module

// b.zm 
TI = time() // time of import ?
if ( @SCRIPT.main ){
  println("From B : " + TI)
}
\end{lstlisting}

Naturally, we can extend an module import with initialisation of arguments, which 
will not be trivial, and hence, there is no need to invent Objects over imported modules.

\end{subsection}

\end{section}

